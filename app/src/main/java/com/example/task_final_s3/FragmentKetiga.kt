package com.example.task_final_s3

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.task_final_s3.databinding.FragmentKetigaBinding

class FragmentKetiga : Fragment() {


    private var _binding: FragmentKetigaBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentKetigaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity()?.let { requireActivity().getWindow().setStatusBarColor(it.getColor(R.color.pinki)) }
        }

        val iniNama = FragmentKetigaArgs.fromBundle(arguments as Bundle).name
        binding.tvNama.text = iniNama

        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<IniDataOrang>("Orang")
            ?.observe(viewLifecycleOwner){ result->
                binding.tvUsia.visibility = View.VISIBLE
                val umuran = result.usia.toInt()
                if (umuran % 2 == 0) {
                    binding.tvUsia.text = "${result.usia}, Genap"
                } else {
                    binding.tvUsia.text = "${result.usia}, Ganjil"
                }
                binding.tvAlamat.visibility = View.VISIBLE
                binding.tvAlamat.text = result.alamat
                binding.tvPekerjaan.visibility = View.VISIBLE
                binding.tvPekerjaan.text = result.pekerjaan
            }

        binding.btnFragmentKeempat.setOnClickListener {
            it.findNavController().navigate(R.id.action_fragmentKetiga_to_fragmentKeempat)
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }





}
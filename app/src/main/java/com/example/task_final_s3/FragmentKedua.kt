package com.example.task_final_s3

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.task_final_s3.databinding.FragmentKeduaBinding

class FragmentKedua : Fragment() {

    private var _binding: FragmentKeduaBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentKeduaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity()?.let { requireActivity().getWindow().setStatusBarColor(it.getColor(R.color.oren_bagus)) }
        }

        binding.btnToFragmentKetiga.setOnClickListener {
            if (binding.textInputEditText.text.isNullOrEmpty()){
                Toast.makeText(requireContext(),"Namanya belum ada :)", Toast.LENGTH_LONG).show()
            } else{
                val actionToFragmentKetiga =
                    FragmentKeduaDirections.actionFragmentKeduaToFragmentKetiga()
                actionToFragmentKetiga.name = binding.textInputEditText.text.toString()
                view.findNavController().navigate(actionToFragmentKetiga)
            }
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }



}
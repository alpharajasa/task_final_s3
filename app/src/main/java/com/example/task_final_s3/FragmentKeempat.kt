package com.example.task_final_s3

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.task_final_s3.databinding.FragmentKeempatBinding
import com.example.task_final_s3.databinding.FragmentKetigaBinding

class FragmentKeempat : Fragment() {


    private var _binding: FragmentKeempatBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentKeempatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity()?.let { requireActivity().getWindow().setStatusBarColor(it.getColor(R.color.unyu)) }
        }
        binding.textInputEditTextUsia.doOnTextChanged { text, start, before, count ->
            if (text!!.length > 2){
                binding.textInputLayoutUsia.error = "Kebanyakan!!"
            }else if (text.length < 3){
                binding.textInputLayoutUsia.error = null
            }
        }


        binding.btnBackToFragmentKetiga.setOnClickListener {
            if (binding.textInputEditTextUsia.text!!.isNotEmpty() && binding.textInputEditTextAlamat.text!!.isNotEmpty() && binding.textInputEditTextPekerjaan.text!!.isNotEmpty()) {

                val usia = binding.textInputEditTextUsia.text.toString()
                val alamat = binding.textInputEditTextAlamat.text.toString()
                val pekerjaan = binding.textInputEditTextPekerjaan.text.toString()
                val orang = IniDataOrang(usia, alamat, pekerjaan)
                findNavController().previousBackStackEntry?.savedStateHandle?.set("Orang", orang)
                findNavController().navigateUp()
            }else{
                Toast.makeText(requireContext(),"Yang lengkap dong :)", Toast.LENGTH_LONG).show()
            }

//            it.findNavController().navigate(R.id.action_fragmentKeempat_to_fragmentKetiga)
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}


package com.example.task_final_s3

import android.os.Parcel
import android.os.Parcelable


data class IniDataOrang(

    val usia: String,
    val alamat: String,
    val pekerjaan: String,

) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(usia)
        parcel.writeString(alamat)
        parcel.writeString(pekerjaan)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<IniDataOrang> {
        override fun createFromParcel(parcel: Parcel): IniDataOrang {
            return IniDataOrang(parcel)
        }

        override fun newArray(size: Int): Array<IniDataOrang?> {
            return arrayOfNulls(size)
        }
    }
}

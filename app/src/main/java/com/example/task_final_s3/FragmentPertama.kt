package com.example.task_final_s3

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.task_final_s3.databinding.FragmentPertamaBinding

class FragmentPertama : Fragment() {
    private var _binding: FragmentPertamaBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPertamaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getActivity()?.let { requireActivity().getWindow().setStatusBarColor(it.getColor(R.color.ijo_bagus)) }
        }

        binding.btnFragmentPertama.setOnClickListener {
            it.findNavController().navigate(R.id.action_fragmentPertama_to_fragmentKedua)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}